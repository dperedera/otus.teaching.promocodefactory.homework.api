﻿using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GRpc
{
    public class GrpcCustomersService : RpcCustomersService.RpcCustomersServiceBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public GrpcCustomersService(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public override async Task<RpcCustomerResponse> GetCustomer(RpcUUIDRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(request.GetGuid());

            var result = customer.ToRpcCustomerResponse();

            return result;
        }


        public override async Task GetCustomers(RpcEmptyRequest request, IServerStreamWriter<RpcCustomerShortResponse> responseStream,
            ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();

            var tasks = customers.Select(x => responseStream.WriteAsync(x.ToRpcCustomerShortResponse()));

            await Task.WhenAll(tasks);
        }

        public override async Task<RpcCustomerResponse> CreateCustomer(RpcCreateCustomerRequest request, ServerCallContext context)
        {
            var preferencesIds = request.Preferences.Select(x => x.GetGuid()).ToList();
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(preferencesIds);

            var customer = request.MapToCustomer(preferences);

            await _customerRepository.AddAsync(customer);

            return customer.ToRpcCustomerResponse();
        }

        public override async Task<RpcStatusResponse> DeleteCustomer(RpcUUIDRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(request.GetGuid());

            if (customer == null)
            {
                return new RpcStatusResponse {Status = StringResponseConstants.EntryNotFound};
            }

            await _customerRepository.DeleteAsync(customer);

            return new RpcStatusResponse {Status = StringResponseConstants.Ok};
        }

        public override async Task<RpcStatusResponse> EditCustomer(RpcEditCustomerRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(request.GetId());

            if (customer == null)
            {
                return new RpcStatusResponse {Status = StringResponseConstants.EntryNotFound};
            }

            var preferencesIds = request.Preferences.Select(x => x.GetGuid()).ToList();
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(preferencesIds);

            customer = request.MapToCustomer(preferences);

            await _customerRepository.UpdateAsync(customer);

            return new RpcStatusResponse {Status = StringResponseConstants.Ok};
        }

        private static class StringResponseConstants
        {
            public const string EntryNotFound = "EntryNotFound";
            public const string Ok = "Ok";
        }
    }
}