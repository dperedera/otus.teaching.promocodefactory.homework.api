﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GRpc
{
    internal static class RpcMessageExtensions
    {
        public static Guid GetGuid(this RpcUUIDRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Value))
            {
                throw new ArgumentNullException(nameof(request.Value));
            }

            return Guid.Parse(request.Value);
        }

        public static Guid GetId(this RpcEditCustomerRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Id))
            {
                throw new ArgumentNullException(nameof(request.Id));
            }

            return Guid.Parse(request.Id);
        }

        public static RpcCustomerResponse ToRpcCustomerResponse(this Customer customer)
        {
            var result = new RpcCustomerResponse
            {
                Id = customer.Id.ToString(),
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName
            };

            if (customer.Preferences != null && customer.Preferences.Any())
            {
                result.Preferences.AddRange(customer.Preferences.Select(x => new RpcCustomerPreferenceResponse
                {
                    Id = x.PreferenceId.ToString(),
                    Name = x.Preference.Name
                }));
            }

            return result;
        }

        public static RpcCustomerShortResponse ToRpcCustomerShortResponse(this Customer customer)
        {
            return new RpcCustomerShortResponse
            {
                Id = customer.Id.ToString(),
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName
            };
        }

        public static Customer MapToCustomer(this RpcCreateCustomerRequest request, IEnumerable<Preference> preferences)
        {
            var id = Guid.NewGuid();

            return new Customer
            {
                Id = id,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Preferences = preferences.Select(x => new CustomerPreference
                {
                    CustomerId = id,
                    Preference = x,
                    PreferenceId = x.Id
                }).ToList()
            };
        }

        public static Customer MapToCustomer(this RpcEditCustomerRequest request, IEnumerable<Preference> preferences)
        {
            var id = request.GetId();

            return new Customer
            {
                Id = id,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Preferences = preferences.Select(x => new CustomerPreference
                {
                    CustomerId = id,
                    Preference = x,
                    PreferenceId = x.Id
                }).ToList()
            };
        }
    }
}